use std::io;
use regex::{Regex, Captures};
use chrono::prelude::*;
use clap::Parser;

#[derive(Parser, Debug)]
#[command(version, about, long_about=None)]
struct Args {
    #[arg(short, long, default_value_t=String::from("%Y-%m-%d %H:%M:%S"))]
    format: String,
}

fn replacer(cap: &Captures, format: &str) -> String {
    let mut result = String::new();

    // bit shit but static start/end times
    let start: DateTime<Utc> = Utc.with_ymd_and_hms(2000, 1, 1, 0, 0, 0).unwrap();
    let end: DateTime<Utc>   = Utc.with_ymd_and_hms(2050, 1, 1, 0, 0, 0).unwrap();

    // now here, modify to UTC, check if it's in range, return
    let phrase = cap.get(0).unwrap().as_str();
    let maybe_timestamp = str::parse::<i64>( phrase ).unwrap();
    let mut sel_time: Option<DateTime<Utc>> = None;
    for candidate_time in [
        DateTime::from_timestamp( maybe_timestamp, 0 ),                   // sec
        DateTime::from_timestamp_millis( maybe_timestamp ),               // ms
        Some(DateTime::from_timestamp_nanos( maybe_timestamp * 1_000 )),  // us
        Some(DateTime::from_timestamp_nanos( maybe_timestamp )),          // ns
    ] {
        if let Some(candidate_time) = candidate_time {
            if (start <= candidate_time) && (candidate_time <= end) {
                sel_time = Some(candidate_time);
                break;
            }
        }
    }

    if let Some(time) = sel_time {
        result.push_str( &time.format(format).to_string() );
    }
    else {
        result.push_str( phrase );
    }

    result
}

fn main() {
    let args = Args::parse();

    let pattern = Regex::new(r"(\d+)").unwrap();

    for line in io::stdin().lines() {
        // try and replace it?
        let line = line.unwrap();
        let replaced = pattern.replace_all(
            &line,
            |cap: &Captures| replacer(cap, &args.format)
        );
        println!("{}", replaced);
    };
}
